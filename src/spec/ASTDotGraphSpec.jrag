/*
ASTGraphSpec specifies how AST node should be formatted
*/

aspect ASTGraphSpec {
	syn String ASTNode.dotSpecIntro() = "digraph {\n    node [shape=box,ordering=out];\n";
	syn String ASTNode.dotSpecOutro() = "}";

	syn String ASTNode.ASTDotNode(ASTNode rootNode) {
		StringBuffer s = new StringBuffer();
		s.append("    ").append(nodeName(rootNode)).append(" [");
		s.append(dotAttributes(rootNode)).append("];\n");
		return s.toString();
	}

	public String ASTNode.ASTDotSpec() {
		StringBuffer s = new StringBuffer();
		s.append(dotSpecIntro()).append(ASTDotTree(this))
		.append(dotSpecOutro());
		return s.toString();
	}

	public String ASTNode.ASTDotTree(ASTNode rootNode) {
		StringBuffer s = new StringBuffer();
		s.append(ASTDotNode(rootNode));
		for(int i = 0; i < getNumChild(); i++) {
			s.append("    ").append(nodeName(rootNode)).append(" -> ");
			s.append(getChild(i).nodeName(rootNode));
			s.append(" [dir=none,style=dotted,weight=10];\n");
			s.append(getChild(i).ASTDotTree(rootNode));
		}
		return s.toString();
	}


	syn String ASTNode.nodeName(ASTNode rootNode) {
		if(this == rootNode){
			return initNodeName();
		}else{
			return computeNodeName(rootNode);
		}
	}  
		
	syn String ASTNode.initNodeName(){
		String nodeName = this.getClass().getSimpleName() + "_1";
		return nodeName;
	}

	syn String ASTNode.computeNodeName(ASTNode rootNode) circular [initNodeName()];

	eq ASTNode.computeNodeName(ASTNode rootNode) {
		String name = computeNodeName(rootNode);
		ASTNode node = rootNode.lookupNodeName(name, rootNode);
		if (node != this && node != null) {
			int underscoreIndex = name.lastIndexOf('_');
			int indexSuffix = Integer.parseInt(name.substring(underscoreIndex + 1));
			return name.substring(0, underscoreIndex  + 1) + (indexSuffix + 1);
		} else {
			return name;
		}
	}

	syn ASTNode ASTNode.lookupNodeName(String name, ASTNode rootNode) {
		String nodeName = nodeName(rootNode);
		if (nodeName.equals(name)) {
			return this;
		}
		for (int j = 0; j < getNumChildInGraph(); j++) {
			ASTNode n = getChildInGraph(j).lookupNodeName(name, rootNode);
			if (n != null) {
				return n;
			}
		}
		return null;
	}

	syn String ASTNode.dotAttributes(ASTNode rootNode) = "label=\"" + nodeLabel(rootNode) + '\"';

	syn String ASTNode.nodeLabel(ASTNode rootNode) {
			String name = nodeName(rootNode);
			return toLabelString();
	}

	public String ASTNode.subGraphString(ASTNode rootNode) {
		ArrayList<ArrayList<ASTNode>> list = new ArrayList<ArrayList<ASTNode>>();
		subGraphs(list, 0);
		StringBuffer s = new StringBuffer();
		for (ArrayList<ASTNode> l : list) {
			if (l.size() > 1) {
				//Write rank subgraphs:
				s.append("    { rank = same; ");	//not required to align if not having same parent
				for (ASTNode node : l) {
					s.append(node.nodeName(rootNode)).append("; ");
				}
				s.append("}\n");
			}
		}
		return s.toString();
	}

	public void ASTNode.subGraphs(ArrayList<ArrayList<ASTNode>> list, int level) {
		if (level >= list.size()) {
			list.add(new ArrayList<ASTNode>());
		}
		list.get(level).add(this);
		for (int i = 0; i < getNumChildInGraph(); i++) {
			getChildInGraph(i).subGraphs(list, level + 1);
		}
	}

	syn int ASTNode.getNumChildInGraph() = this.getNumChild();
	syn ASTNode ASTNode.getChildInGraph(int i) = this.getChild(i);
	syn ASTNode ASTNode.getParentInGraph() = getParent();
}