/*
CFGDotGraphSpec specifies how to format graph related information
*/
aspect CFGDotGraphSpec {

	public String Script.CFGDotSpec() {
		StringBuffer s = new StringBuffer();
		s.append(dotSpecIntro()).append(entry().ASTDotNode(this));
		s.append("    ").append(entry().nodeName(this)).append(" -> ").append(nodeName(this)).append(" [dir=none];\n");

		s.append(ASTDotTree(this)).append('\n');
		s.append(subGraphString(this)).append('\n');	//align nodes within same scope
		s.append(CFGDotTree(this)).append('\n');

		s.append(exit().ASTDotNode(this));
		s.append("    ").append(nodeName(this)).append(" -> ").append(exit().nodeName(this)).append(" [dir=none];\n");
		s.append(entryExitRank(this)).append(dotSpecOutro());
		return s.toString();
	}

	//there is only signle entry & single exit for each procedural context
	eq CFGEntry.nodeName(ASTNode rootNode) = "entry";
	eq CFGExit.nodeName(ASTNode rootNode) = "exit";

	syn String CommandBlock.dotAttributes(ASTNode rootNode) =
	"shape=record, label=\"<name> " + nodeLabel(rootNode) + " | {<pred> pred | <succ> succ}\"";

	//may need to change its type to CommandBlock
	syn String BlockSeq.dotAttributes(ASTNode rootNode) =
	"shape=record, label=\"<name> " + nodeLabel(rootNode) + " | {<pred> pred | <succ> succ}\"";

	syn String ControlFlowBlock.dotAttributes(ASTNode rootNode) =
	"shape=record, label=\"<name> " + nodeLabel(rootNode) + " | {<pred> pred | <succ> succ}\"";

	syn String Expression.dotAttributes(ASTNode rootNode) =
	"shape=record, label=\"<name> " + nodeLabel(rootNode) + " | {<pred> pred | <succ> succ}\"";

	syn String Parameter.dotAttributes(ASTNode rootNode) =
	"shape=record, label=\"<name> " + nodeLabel(rootNode) + " | {<pred> pred | <succ> succ}\"";

	syn String BlockExp.dotAttributes(ASTNode rootNode) =
	"shape=record, label=\"<name> " + escapeMetaCharacters(nodeLabel(rootNode)) + " | {<pred> pred | <succ> succ}\"";

	syn String CFGEntry.dotAttributes(ASTNode rootNode) =
	"shape=ellipse, label=\"" + nodeLabel(rootNode) + "\"";
	syn String CFGExit.dotAttributes(ASTNode rootNode) =
	"shape=ellipse, label=\"" + nodeLabel(rootNode) + "\"";

	String BlockExp.escapeMetaCharacters(String inputString) {
		final String[] metaCharacters = {"\\", "^", "$", "{", "}", "[", "]", "(", ")", ".", "*", "+", "?", "|", "<", ">", "-", "&"};
		String outputString = "";
		for (int i = 0 ; i < metaCharacters.length ; i++) {
			if (inputString.contains(metaCharacters[i])) {
				outputString = inputString.replace(metaCharacters[i], "\\" + metaCharacters[i]);
				inputString = outputString;
			}
		}
		return outputString;
	}

	syn CFGNode Script.getFirstCFGNode();
	eq Script.getFirstCFGNode() = (CFGNode) getBody().getBlocks(0);
	eq ProcDef.getFirstCFGNode() {
		if (getNumFormalParams() > 0) {
			return (CFGNode) getFormalParams(0);
		}
		if (getBody().getNumBlocks() > 0) {
			return (CFGNode) getBody().getBlocks(0);
		}
		return null;
	}

	syn String Script.CFGDotTree(ASTNode rootNode) {
		StringBuffer res = new StringBuffer();
		SmallSet<CFGNode> visitedList = SmallSet.mutable();
		ArrayList<CFGNode> worklist = new ArrayList<CFGNode>();
		CFGEntry entry = entry();
		CFGExit exit = exit();
		worklist.add(entry);
		visitedList.add(entry);

		while (!worklist.isEmpty()) {
			CFGNode node = worklist.remove(0);
			res.append(node.succToString(rootNode));
			Iterator<CFGNode> itr = node.succ().iterator();
			while (itr.hasNext()) {
				CFGNode succ = itr.next();
				if (!visitedList.contains(succ)
				        && succ != exit
				   ) {
					worklist.add(succ);
					visitedList.add(succ);
				}
			}
		}

		return res.toString();
	}

	syn String CFGNode.succToString(ASTNode rootNode);

	syn String CFGNode.entryPortName(ASTNode rootNode);
	syn String CFGNode.exitPortName(ASTNode rootNode);

	eq CFGNode.entryPortName(ASTNode rootNode) = nodeName(rootNode) + ":name";
	eq CFGNode.exitPortName(ASTNode rootNode) = nodeName(rootNode) + ":succ";
	eq CFGEntry.entryPortName(ASTNode rootNode) = nodeName(rootNode);
	eq CFGEntry.exitPortName(ASTNode rootNode) = nodeName(rootNode);
	eq CFGExit.entryPortName(ASTNode rootNode) = nodeName(rootNode);
	eq CFGExit.exitPortName(ASTNode rootNode) = nodeName(rootNode);

	syn boolean CFGNode.isEntryExit() = false;
	eq CFGEntry.isEntryExit() = true;
	eq CFGExit.isEntryExit() = true;


	eq CFGNode.succToString(ASTNode rootNode) {
		StringBuffer res = new StringBuffer();
		if (succ() == null) {
			System.out.println(this.getClass().getSimpleName());
		}
		for (CFGNode succ : succ()) {
			res.append("    ").append(exitPortName(rootNode)).append(" -> ");
			res.append(succ.entryPortName(rootNode));
			if (getParentInGraph() != succ.getParentInGraph()
			        || isEntryExit() && !succ.isEntryExit() || !isEntryExit() && succ.isEntryExit()
			   ) {
				res.append(" [constraint=false]");
			}
			res.append(";\n");
		}
		return res.toString();
	}

	refine ASTDotGraphSpec public String ASTNode.subGraphString(ASTNode rootNode) {
		StringBuffer s = new StringBuffer(ASTDotGraphSpec.ASTNode.subGraphString(rootNode));
		ArrayList<ArrayList<ASTNode>> list = new ArrayList<ArrayList<ASTNode>>();
		subGraphs(list, 0);
		for (ArrayList<ASTNode> l : list) {
			if (l.size() > 1) {
				//Write invisible stabilizing edges:
				boolean rigid = true;
				s.append(invisEdgeString(l, rigid, rootNode));
			}
		}
		return s.toString();
	}

	public String ASTNode.invisEdgeString(ArrayList<ASTNode> l, boolean connectAll, ASTNode rootNode) {
		StringBuffer s = new StringBuffer();
		boolean isAtHead = false;
		boolean wroteEdge = false;
		boolean indent = true;
		for (int i = 0; i < l.size(); i++) {
			ASTNode node = l.get(i);
			ASTNode nextNode = i < l.size() - 1 ? l.get(i + 1) : null;
			boolean connectSiblings = connectAll || nextNode != null && node.isSibling(nextNode);
			if (isAtHead || nextNode != null && connectSiblings && !node.isConnectedToSibling()) {
				if (indent) {
					s.append("    ");
					indent = false;
				}
				s.append(node.nodeName(rootNode));
				isAtHead = false;
				wroteEdge = true;
			}
			if (nextNode != null && connectSiblings && !node.isConnectedToSibling()) {
				s.append(" -> ");
				isAtHead = true;
			}
			if ((nextNode == null || nextNode != null && !connectSiblings || node.isConnectedToSibling())
			        && wroteEdge) {
				s.append(" [style=invis];\n");
				indent = true;
				wroteEdge = false;
			}
		}
		return s.toString();
	}


	inh boolean ASTNode.isSibling(ASTNode node);
	eq ASTNode.getChild(int i).isSibling(ASTNode node) = getChildInGraph(i).getParentInGraph() == node.getParentInGraph();

	syn boolean ASTNode.isConnectedToSibling() = false;
	syn boolean CFGNode.isConnectedToSibling() {
		boolean a = succ().contains(getNextSibling());
		boolean b = pred().contains(getNextSibling());
		return a || b;
	}

	inh ASTNode CFGNode.getNextSibling();
	eq ASTNode.getChild(int i).getNextSibling() = getNumChildInGraph() == i + 1 ? null : getChildInGraph(i + 1);
	inh ASTNode CFGNode.getPreviousSibling();
	eq ASTNode.getChild(int i).getPreviousSibling() = i < 1 ? null : getChildInGraph(i - 1);

	//to make the entry -- root -- exit be in the same level
	syn lazy String ASTNode.entryExitRank(ASTNode rootNode) = "";
	syn String Script.entryExitRank(ASTNode rootNode) = "    { rank = same; " + entry().nodeName(rootNode) + "; " + exit().nodeName(rootNode) + "; " + nodeName(rootNode) + "; }\n";

}