import java.util.ArrayList;
import java.util.HashMap;

aspect UnreachableScriptAnalysis {

	inh Program Scriptable.program();
	eq Program.getStage().program() = this;

	coll HashSet<BroadcastBlock> Script.messageSenders() [new HashSet()] with add;
	BroadcastBlock contributes this
	to Script.messageSenders()
	for this.parentScript();


	coll HashSet<BroadcastBlock> Scriptable.messageSenders() [new HashSet()] with addAll;

	Script contributes messageSenders()
	to Scriptable.messageSenders()
	for this.spriteParent();

	coll HashSet<BroadcastBlock> Program.allMessageSenders() [new HashSet()] with addAll;

	Scriptable contributes messageSenders()
	to Program.allMessageSenders()
	for program();


	coll HashSet<WhenIReceiveBlock> Scriptable.messageReceivers() [new HashSet()] with add;

	WhenIReceiveBlock contributes this
	to Scriptable.messageReceivers()
	for this.spriteParent();

	coll HashSet<WhenIReceiveBlock> Program.allMessageReceivers() [new HashSet()] with addAll;

	Scriptable contributes messageReceivers()
	to Program.allMessageReceivers()
	for program();

	syn String MessageBlock.getMessage() {
		StringLiteral strLit = (StringLiteral)getArgument(0);
		return strLit.getValue();
	}

	syn boolean CommandBlock.isEventHatBlock() {
		String[] eventHatBlocks = new String[] {"whenGreenFlag", "whenIReceive", "whenKeyPressed",
			"whenClicked", "whenSceneStarts", "whenSensorGreaterThan", "whenCloned", "whenIReceive"
		};
		java.util.List<String> listOfEventBlocks = Arrays.asList(eventHatBlocks);
		if (listOfEventBlocks.contains(this.getBlockName())) {
			return true;
		} else {
			return false;
		}
	}

	coll HashSet<CommandBlock> Program.allEventHatBlocksColl() [new HashSet()] with add;

	CommandBlock contributes this
	to Program.allEventHatBlocksColl()
	for spriteParent().program();

	syn HashSet<BroadcastBlock> WhenIReceiveBlock.getSenders() = getSenderOf(this);

	inh HashSet<BroadcastBlock> WhenIReceiveBlock.getSenderOf(WhenIReceiveBlock receiver);

	eq Program.getStage().getSenderOf(WhenIReceiveBlock receiver) {
		return getSenderOf(receiver);
	}

	syn HashSet<BroadcastBlock> Program.getSenderOf(WhenIReceiveBlock receiver) {
		String message = receiver.getMessage();
		HashSet<BroadcastBlock> senders = new HashSet<>();
		for (BroadcastBlock block : allMessageSenders()) {
			if (block.getMessage().equals(message)) {
				senders.add(block);
			}
		}
		return senders;
	}



	syn HashSet<MessageBlock> BroadcastBlock.getReceivers() = getReceiverOf(this);

	inh HashSet<MessageBlock> BroadcastBlock.getReceiverOf(BroadcastBlock sender);

	eq Program.getStage().getReceiverOf(BroadcastBlock sender) {
		return getReceiverOf(sender);
	}

	syn HashSet<MessageBlock> Program.getReceiverOf(BroadcastBlock sender) {
		String message = sender.getMessage();
		HashSet<MessageBlock> receivers = new HashSet<>();
		for (MessageBlock block : allMessageReceivers()) {
			if (block.getMessage().equals(message)) {
				receivers.add(block);
			}
		}
		return receivers;
	}

	syn HashSet<MessageBlock> MessageBlock.getReceivers() = new HashSet<MessageBlock>();

	syn Set<MessageBlock> MessageBlock.reachable() circular [new HashSet<MessageBlock>()];
	eq MessageBlock.reachable() {
		HashSet<MessageBlock> result = new HashSet<MessageBlock>();
		for (MessageBlock receiver : getReceivers()) {
			result.add(receiver);
			for (MessageBlock sender : receiver.parentScript().messageSenders()) {
				result.addAll(sender.reachable());
			}
		}
		return result;
	}

}