package analysis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import analysis.ast.Scriptable;
import analysis.ast.Sprite;
import analysis.parsing.Util;

public class ProgramInfoAnalysis extends Analysis {
	
	
	@Override
	public void initialize() {
		// TODO Auto-generated method stub

	}

	@Override
	public void performAnalysis() {

	}



	@Override
	protected void buildFullReport() {

	}

	@Override
	protected void buildShortReport() {
		shortReport.put("name", "COSTUMES");
		for(Scriptable s : program.getAllScriptables()) {
			shortReport.put(s.getName(), s.getCostume(0).getValue());
		}
	}

	

}
