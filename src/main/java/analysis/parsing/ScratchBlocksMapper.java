package analysis.parsing;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import analysis.smell.cloneutils.RelevantASTNode;
import main.Main;

public class ScratchBlocksMapper {
	private static Map<String, BlockSpec> blockMap = new HashMap<String, BlockSpec>();
	public final static Map<String, Integer> name2id = new HashMap<String, Integer>();
	public final static Map<Integer, String> id2name = new HashMap<Integer, String>();
	static int ID = 0;
	static String SB2_SPEC_FILEPATH = "/block-specs.json";

	// sb2 to sb3
	public final static Map<String, String> sb2tosb3 = new HashMap<String, String>();
	public final static Map<String, String> sb3tosb2 = new HashMap<String, String>();

	static String SB2_TO_SB3_SPEC_FILEPATH = "/sb2-to-sb3-mapping.json";

	static {
		JsonParser parser = new JsonParser();
		JsonElement jsonTree;

		jsonTree = getJsonElementFromFile(parser, SB2_SPEC_FILEPATH);
		parseSb2Spec(jsonTree);
		constructTwoWayMapIDName();

		// parse sb2 to sb3 mapping
		jsonTree = getJsonElementFromFile(parser, SB2_TO_SB3_SPEC_FILEPATH);
		parseSb2ToSb3Map(jsonTree);

	}

	// used in clone computation
	private static void constructTwoWayMapIDName() {
		SortedSet<String> keys = new TreeSet<String>(blockMap.keySet());
		for (String astNodeName : RelevantASTNode.relevantASTNodeNames) {
			keys.add(astNodeName);
		}

		for (String key : keys) {
			name2id.put(key, ID);
			id2name.put(ID, key);
			ID++;
		}
	}

	private static void parseSb2ToSb3Map(JsonElement jsonTree) {
		JsonObject jsonMap = jsonTree.getAsJsonObject();
		for (Entry<String, JsonElement> entry : jsonMap.entrySet()) {
			String sb3Opcode = entry.getValue().getAsJsonObject().get("opcode").getAsString();
			sb2tosb3.put(entry.getKey(), sb3Opcode);
			sb3tosb2.put(sb3Opcode, entry.getKey());
		}

	}

	public static JsonElement getJsonElementFromFile(JsonParser parser, String jsonFilePath) {
		BufferedReader br;
		InputStreamReader dataReader;
		JsonElement jsonTree;
		JsonElement result = null;
		URL sb2Url = Main.class.getResource(jsonFilePath);
		try {
			dataReader = new InputStreamReader(sb2Url.openStream());
			br = new BufferedReader(dataReader);
			result = parser.parse(br);
			dataReader.close();
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

		jsonTree = result;
		return jsonTree;
	}

	private static void parseSb2Spec(JsonElement jsonTree) {
		// parse sb2 spec
		JsonArray array = jsonTree.getAsJsonArray();
		for (int i = 0; i < array.size(); i++) {
			JsonArray specArray = array.get(i).getAsJsonArray();
			if (specArray.size() < 4) {
				continue;
			}
			BlockSpec spec = new BlockSpec();
			spec.setFormat(reformat(specArray.get(0).getAsString()));
			spec.setBlockType(fullBlockTypeOf(specArray.get(1).getAsString()));
			spec.setBlockName(specArray.get(3).getAsString());
			spec.setArgTypes(extractArgTypes(specArray.get(0).getAsString()));
			blockMap.put(specArray.get(3).getAsString(), spec);
		}
	}

	public static int getIDFromName(String name) {
		if (name2id.containsKey(name)) {
			return name2id.get(name);
		} else {
			id2name.put(ID, name);
			name2id.put(name, ID);
			ID++;
			return name2id.get(name);
		}
	}

	public static String getFormatOf(String scratchBlockName) {
		if (blockMap.containsKey(scratchBlockName)) {
			return blockMap.get(scratchBlockName).getFormat();
		} else {
			return reformat(scratchBlockName); // undefined
		}
	}

	public static String getBlockType(String scratchBlockName) {
		if (blockMap.containsKey(scratchBlockName)) {
			return blockMap.get(scratchBlockName).getBlockType();
		} else {
			return "undefined";
		}
	}

	public static String fullBlockTypeOf(String blockType) {
		String fullType = "";
		switch (blockType) {
		case "b":
			fullType = "Boolean";
			break;
		case "r":
			fullType = "Reporter";
			break;
		default:
			fullType = "stack";
			break;
		}
		return fullType;
	}

	public static String[] getArgTypes(String scratchBlockName) {
		if (blockMap.containsKey(scratchBlockName)) {
			return blockMap.get(scratchBlockName).getArgTypes();
		} else { // undefined
			return extractArgTypes(scratchBlockName);
		}
	}

	public static String[] extractArgTypes(String asString) {
		Pattern p = Pattern.compile("\\%(Z|n|b|d|s|c|m.(\\w)+|x.(\\w)+)");
		Matcher m = p.matcher(asString);
		List<String> types = new ArrayList<String>();
		String type;
		while (m.find()) {
			switch (m.group().substring(0, 2)) { // first two character %X
			case "%b":
				type = "Boolean";
				break;
			case "%c":
				type = "Color";
				break;
			case "%d":
				type = "Number-menu";
				break;
			case "%m":
				type = "Readonly-menu";
				break;
			case "%n":
				type = "Number";
				break;
			case "%s":
				type = "String";
				break;
			case "%x":
				type = "Inline";
				break;
			case "%Z":
				type = "Block";
				break;
			default:
				type = "String";
				break;
			}
			types.add(type);
		}
		String[] typeOrdering = types.toArray(new String[types.size()]);
		return typeOrdering;
	}

	public static String reformat(String format) {
		String reformat = format.replaceAll("\\%(Z|n|b|d.(\\w)+|c|m.(\\w)+|x.(\\w)+)", "%s");
		return reformat;
	}

	public static String customBlockReformat(String format) {
		String reformat = format.replaceAll("\\%(Z|n|d|c|m.(\\w)+|x.(\\w)+)", "%s");
		reformat = reformat.replaceAll("\\%b", "\\\\ %s");
		return reformat;
	}

}
