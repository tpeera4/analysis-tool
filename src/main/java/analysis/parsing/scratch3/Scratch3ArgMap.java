package analysis.parsing.scratch3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import analysis.parsing.ScratchBlocksMapper;

public class Scratch3ArgMap {
	static String SB2_TO_SB3_SPEC_FILEPATH = "/sb2-to-sb3-mapping.json";
	private static Map<String, Scratch3BlockSpec> blockMap = new HashMap<String, Scratch3BlockSpec>();
	
	static {
//		JsonParser parser = new JsonParser();
//		JsonElement jsonTree;
//		jsonTree = ScratchBlocksMapper.getJsonElementFromFile(parser, SB2_TO_SB3_SPEC_FILEPATH);
		
	}
	
	private static void parseSb3BlockSpec(JsonElement jsonTree) {
		JsonObject jsonMap = jsonTree.getAsJsonObject();
		for (Entry<String, JsonElement> entry : jsonMap.entrySet()) {
			String sb3Opcode = entry.getValue().getAsJsonObject().get("opcode").getAsString();
			Scratch3BlockSpec spec = new Scratch3BlockSpec();
			spec.blockName = "";
			spec.opcode = "";
			System.out.println(entry.getValue().getAsJsonObject().toString());
//			
			JsonArray argMap = entry.getValue().getAsJsonObject().get("argMap").getAsJsonArray();
			for(int argIdx = 0; argIdx < argMap.size(); argIdx++) {
				System.out.println(argMap.get(argIdx).toString());
				Scratch3BlockSpec.ArgSpec argSpec = spec.new ArgSpec();
				JsonObject argObj = argMap.get(argIdx).getAsJsonObject();
				argSpec.inputName = argObj.get("inputName").getAsString();
				argSpec.inputOp = argObj.get("inputOp").getAsString();;
				argSpec.type = argObj.get("type").getAsString();;
			}
			
			
			blockMap.put(entry.getKey(), spec);
			
			
		}

	}
	
	public static void main(String[] args) {
		JsonParser parser = new JsonParser();
		JsonElement jsonTree;
		jsonTree = ScratchBlocksMapper.getJsonElementFromFile(parser, SB2_TO_SB3_SPEC_FILEPATH);
		Scratch3ArgMap.parseSb3BlockSpec(jsonTree);
	}
	

}
