package analysis.parsing.scratch3;

import analysis.ast.Program;
import analysis.ast.Scriptable;
import analysis.ast.Sprite;

public class ScratchBlocks3Util {

	public static Scriptable getSpriteFromWorkSpaceId(Program program, String workspaceId) {

		Scriptable scriptable = program.getScriptableFromWorkSpaceID(workspaceId);
		if (scriptable != null) {
			return scriptable;
		}
		// see if stage or sprite?
		// stage?

		// sprite?
		scriptable = new Sprite();
		scriptable.setWorkSpaceID(workspaceId);
		scriptable.setName("Sprite1"); // set based on JSON
		program.getStage().addSprite((Sprite) scriptable);

		return scriptable;
	}
}
