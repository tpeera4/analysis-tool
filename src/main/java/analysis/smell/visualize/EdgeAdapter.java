package analysis.smell.visualize;

import java.lang.reflect.Type;
import java.util.Collection;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import com.google.gson.JsonSerializer;
import com.google.gson.reflect.TypeToken;

public class EdgeAdapter implements JsonSerializer<Edge>{

	@Override
	public JsonElement serialize(Edge edge, Type type, JsonSerializationContext ctx) {
		JsonObject obj = new JsonObject();
        obj.addProperty("from", edge.from);
        obj.addProperty("to", edge.to);
        obj.addProperty("label", edge.label);
        obj.addProperty("arrows", "to");
        
        //snippet
        Gson gson = new Gson();
        if(edge.properties.containsKey("snippets")) {
        	JsonElement snippets = gson.toJsonTree(edge.properties.get("snippets"), 
        			new TypeToken<Collection<String>>() {}.getType());   
        	obj.add("snippets", snippets);
        }
        
        //other properties
//        for(String key: edge.properties.keySet()) {
//        	obj.addProperty(key, (String) edge.properties.get(key));
//        }
        
        return obj;
	}
	
}
