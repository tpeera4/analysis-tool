package analysis.smell.visualize;

import java.util.HashMap;
import java.util.Map;

import analysis.ast.Costume;

public class Node {
	String id;
	String label;
	String shape;
	String image;
	Map<String, Edge> edgeMap = new HashMap<String, Edge>();
	Map<String, Object> properties = new HashMap<String, Object>();
	
	public Node(String id) {
		this.id = id;
		setLabel(id);
	}

	public void setLabel(String label) {
		this.label = label;
	}

	public void addEdge(Edge edge) {
		if(edge.from.equals(id)) {			
			edgeMap.put(edge.to, edge);
		}else {
			edgeMap.put(edge.from, edge);
		}
	}

	public Node addProperty(String key, Object value) {
		properties.put(key, value);
		return this;
	}
}
