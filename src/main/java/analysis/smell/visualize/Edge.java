package analysis.smell.visualize;

import java.util.HashMap;
import java.util.Map;

public class Edge {
	public Edge(Node from, Node to)  {
		this.from = from.id;
		this.to= to.id;
	}
	public Edge(String from, String to) {
		this.from = from;
		this.to = to;
	}
	String from;
	String to;
	String arrows;
	String label;
	Map<String, Object> properties = new HashMap<String, Object>();
	
	public Edge setLabel(String label) {
		this.label = label;
		return this;
	}
	public Edge addProperty(String key, Object value) {
		properties.put(key, value);
		return this;
	}
	
	
	
	
}
