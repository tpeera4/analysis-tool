package analysis.smell.visualize;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;

public class Network {
	Map<String, Node> nodeMap = new HashMap<String, Node>();
	Set<Edge> edges = new HashSet<Edge>();
	
	
	public Node makeNode(String nodeID) {
		Node node = new Node(nodeID);
		nodeMap.put(nodeID, node);
		return node;
	}
	
	public Node insertNode(Node node) {
		nodeMap.put(node.id, node);
		return node;
	}
	
	
	public Edge makeEdge(String from, String to) {
		Edge edge = new Edge(from, to);
		edges.add(edge);
		nodeMap.get(from).addEdge(edge);
		nodeMap.get(to).addEdge(edge);
		return edge;
	}
	
	public Edge getEdge(String from, String to) {
		return nodeMap.get(from).edgeMap.get(to);
	}
	
	
	static Gson getGson() {
		GsonBuilder gsonBuilder = new GsonBuilder(); 
		gsonBuilder.registerTypeAdapter(Network.class, new NetworkAdapter());
		gsonBuilder.registerTypeAdapter(Node.class, new NodeAdapter());
		gsonBuilder.registerTypeAdapter(Edge.class, new EdgeAdapter());
		return gsonBuilder.create();
	}
	
	public String getNetworkAsJSONString() {
		return getGson().toJson(this);
	}


	public Node getNode(String nodeID) {
		return nodeMap.get(nodeID);
	}


	public JsonObject getNetworkAsJSONObject() {
		return getGson().toJsonTree(this).getAsJsonObject();
	}

	@Override
	public String toString() {
		return "Network [nodeMap=" + nodeMap + ", edges=" + edges + "]";
	}
}
