package analysis.smell;

import java.io.StringReader;
import java.util.Set;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import analysis.ast.Program;
import analysis.ast.VarDecl;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import analysis.smell.BroadVariableScopeAnalysis;

public class BroadVariableScopeTest extends TestCase {
	private final static String FILENAME = "/smell/test-BroadVariableScope.json";
//	private final static int projectID = 151770183;
	private Program program;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}
	public void test(){
		BroadVariableScopeAnalysis analysis = new BroadVariableScopeAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String fullJsonReport = analysis.getFullReportAsJson();
		System.out.println(fullJsonReport);
		JsonObject shortReport = analysis.getShortReportAsJsonObject();
		System.out.println(analysis.getShortReportAsJson());
		assertEquals(25.0, shortReport.get("percentage").getAsDouble(),0.01);
	}
}