package analysis.smell;

import java.io.StringReader;

import com.google.gson.JsonObject;

import junit.framework.TestCase;
import analysis.ast.Program;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import analysis.smell.FeatureEnvyAnalysis;

public class FeatureEnvyAnalysisTest extends TestCase {
	private final static String FILENAME = "/smell/test-FeatureEnvy.json";
//	private final static int projectID = 149974792;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}
	
	/*
	 * sprite2 envious of sprite1
	 * communicate result via variable resultForSprite2
	 * 
	 * stage envious of sprite2 (vis resultForStage)
	 * 
	 */
	
	public void test() {
		FeatureEnvyAnalysis analysis = new FeatureEnvyAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		System.out.println(analysis.getFullReportAsJsonObject());
		JsonObject shortReportObj = analysis.getShortReportAsJsonObject();
		System.out.println(analysis.getShortReportAsJson());
//		assertEquals(1, shortReportObj.get("count").getAsInt());
	}
	
	public void testVisualizationFormat() {
		FeatureEnvyAnalysis analysis = new FeatureEnvyAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		String visOutput = analysis.getVisualizableReport();
		System.out.println(visOutput);
	}
}