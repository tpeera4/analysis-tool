package analysis.flow;

import java.io.StringReader;
import java.util.HashSet;
import java.util.Set;

import analysis.ast.ChangeVarBlock;
import analysis.ast.CommandBlock;
import analysis.ast.Program;
import analysis.ast.ReachingDef;
import analysis.ast.Script;
import analysis.ast.SetVarBlock;
import analysis.ast.Sprite;
import analysis.ast.VarAccess;
import analysis.ast.VarDecl;
import analysis.ast.Variable;
import analysis.ast.VariableExpBlock;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import junit.framework.TestCase;

public class ReachingDefinitionTest extends TestCase {
	private final static String FILENAME = "/cfg/test-CFG.json";
	private final static int projectID = 182217151; // 151770183
	Program program = null;

	@Override
	protected void setUp() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		program = null;

		try {
			srd = Util.retrieveProjectOnline(projectID);
			// srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	public void testReachingDefOfVarDecl() {
		Sprite sprite = program.getStage().getSprite(0);
		VarDecl v = sprite.getVarDecl(0);
		System.out.println(v.reaching_defs());

		SetVarBlock b = (SetVarBlock) sprite.getScript(0).getBody().getBlocks(1);
		VarDecl v1 = b.getVar().decl();
		System.out.println(v1.reaching_defs());
		assertEquals(v, v1);

	}

	public void testPlayground() {

	}

	public void testReachingOutOfSingleFlowPathWithLocalVar() {
		Set<VarAccess> gen = new HashSet<VarAccess>();

		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		ChangeVarBlock setVarBlock = (ChangeVarBlock) script.getBody().getBlocks(3);
		VarAccess setVarBlock_varAcc = setVarBlock.getVar();

		CommandBlock block = (CommandBlock) script.getBody().getBlocks(4);
		VariableExpBlock varExp = (VariableExpBlock) block.getArgument(0);
		VarAccess varExp_varAcc = varExp.getVar();
		VarAccess reachingVarAcc = (VarAccess) varExp_varAcc.reaching_reachingDefs().iterator().next();

		assertEquals(setVarBlock_varAcc, reachingVarAcc);

		// Util.writeToFile("./tmp/test-entry-exit.txt", script.CFGDotSpec());
		// ASTNode node = script.lookupNodeName("List_1", script);
		// System.out.print(node);
	}

	public void testReachingOutOfSingleFlowPathWithGlobalVar() {

	}

	public void testReachingDefOfMultipleFlowPathsWithCondition() {

	}

	public void testReachingDefOfMultipleFlowPathsWithLoop() {

	}

}