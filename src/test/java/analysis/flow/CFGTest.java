package analysis.flow;

import java.io.StringReader;

import junit.framework.TestCase;
import analysis.ast.ASTNode;
import analysis.ast.ChangeVarBlock;
import analysis.ast.CommandBlock;
import analysis.ast.Program;
import analysis.ast.Script;
import analysis.ast.SetVarBlock;
import analysis.ast.Sprite;
import analysis.ast.VariableExpBlock;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;

public class CFGTest extends TestCase {
	private final static String FILENAME = "/cfg/test-CFG.json";
	private final static int projectID = 182217151; //151770183
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	    	  srd = Util.retrieveProjectOnline(projectID);
//	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}
	
	public void testCFG() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0); 
		CommandBlock firstBlock = (CommandBlock) script.getBody().getBlocks(0);
//		System.out.println(script.CFGDotSpec());
//		Util.writeToFile("./tmp/test-entry-exit.txt", script.CFGDotSpec());
		// ASTNode node = script.lookupNodeName("List_1", script);
		// System.out.print(node);
		
	}

}