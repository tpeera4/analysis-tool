package analysis.flow;

import java.io.StringReader;

import analysis.ast.ChangeVarBlock;
import analysis.ast.CommandBlock;
import analysis.ast.Program;
import analysis.ast.Script;
import analysis.ast.SetVarBlock;
import analysis.ast.Sprite;
import analysis.ast.VarDecl;
import analysis.ast.VariableExpBlock;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import junit.framework.TestCase;

public class VarAccessLookupTest extends TestCase {
	private final static String FILENAME = "/cfg/test-VarAccessLookup.json";
//	private final static int projectID = 119170531;
	Program program = null;

	@Override
	protected void setUp() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		program = null;

		try {
//			srd = Util.retrieveProjectOnline(projectID);
			 srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
	
	public void testVarAccessIsDest(){
		Sprite sprite = program.getStage().getSprite(1);
		Script script = sprite.getScript(0); 

		SetVarBlock setVarBlock = (SetVarBlock) script.getBody().getBlocks(0);
		assertTrue(setVarBlock.getVar().isDest());
		
		ChangeVarBlock changeVarBlock = (ChangeVarBlock) script.getBody().getBlocks(1);
		assertTrue(changeVarBlock.getVar().isDest());
		
		CommandBlock moveBlock = (CommandBlock) script.getBody().getBlocks(2);
		VariableExpBlock varExpBlock = (VariableExpBlock) moveBlock.getArgument(0);
		assertFalse(varExpBlock.getVar().isDest());
	}

	public void testLookupLocalVarDecl() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(0);
		VariableExpBlock varExpBlk = (VariableExpBlock) script.getBody().getBlocks(0);
		VarDecl varDecl = sprite.getVarDecl(0);
		assertEquals(varDecl, varExpBlk.getVar().decl());
	}

	public void testLookupGlobalVarDecl() {
		Sprite sprite = program.getStage().getSprite(0);
		Script script = sprite.getScript(1);
		VariableExpBlock varExpBlk = (VariableExpBlock) script.getBody().getBlocks(0);
		VarDecl varDecl = program.getStage().getVarDecl(0);
		assertEquals(varDecl, varExpBlk.getVar().decl());
	}

	public void testLookupLocalShadowGlobalVarDecl() {
		// Scratch does not allow local & global variable with the same name
	}
}