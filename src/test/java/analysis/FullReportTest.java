package analysis;

import java.io.StringReader;

import analysis.ast.Costume;
import analysis.ast.Program;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import junit.framework.TestCase;
import main.AnalysisManager;

public class FullReportTest extends TestCase {
	private final static String FILENAME = "/test-input.txt";
	private final static int projectID = 149974792;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
	    	  srd = Util.retrieveProjectOnline(projectID);
//	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}

	public void testParseCostumeInfo() {
		Costume costume = program.getStage().getSprite(0).getCostume(0);
		assertNotNull(costume.getValue());
	}
	
	public void testFullAnalysisReportHasCostumeInfo() {
		String fullJsonReport = AnalysisManager.process(program);
		System.out.println(fullJsonReport);
	}

	
	
}