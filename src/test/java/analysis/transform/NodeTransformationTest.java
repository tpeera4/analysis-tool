package analysis.transform;

import analysis.ast.CommandBlock;
import analysis.ast.Expression;
import analysis.ast.Program;
import analysis.ast.ScratchBlock;
import analysis.ast.Script;
import analysis.ast.Scriptable;
import analysis.ast.SetVarBlock;
import analysis.ast.StringLiteral;
import analysis.parsing.scratch3.Scratch3XMLParser;
import junit.framework.TestCase;

public class NodeTransformationTest extends TestCase {

	/*
	 * Action action = target.declareVar(varName, initialVal); action { type:
	 * 'var_decl', var_name: 'name', var_id: 'id', initial_val : 'value' }
	 */
	public void testDeclareGlobalVar() {
		Program program = ProgramUtil.createProgram();
		assertEquals(0, program.variableDeclarations().size());

		program.getStage().addVarDecl("name", "ID", "init");
		assertEquals(1, program.variableDeclarations().size());
	}

	/*
	 * BlocksUtil.createBlock(String blockName, List<Object> args);
	 */
	public void testCreateSetVarBlock() throws Exception {
		Expression val = new StringLiteral();
		((StringLiteral) val).setValue("A");
		SetVarBlock setVar = SetVarBlock.create("BLOCK_ID", "VAR_NAME","VAR_ID", val);
		assertEquals("set [VAR_NAME v] to [A]", setVar.render());

		String expBlockXML = "<block type=\"operator_round\" id=\"(t84=rD5[5E70gDrd`|i\">\n"
				+ "        <value name=\"NUM\">\n"
				+ "          <shadow type=\"math_number\" id=\"qgEn@LdX`X}mQCl-4=dL\">\n"
				+ "            <field name=\"NUM\">5</field>\n" + "          </shadow>\n" + "        </value>\n"
				+ "      </block>";

		Expression blockExp = Scratch3XMLParser.parseExpressionFromXML(expBlockXML);
		SetVarBlock setVar2 = SetVarBlock.create("BLOCK_ID", "VAR_NAME", "VAR_ID", blockExp);
		assertEquals("set [VAR_NAME v] to (round (5.0))", setVar2.render());

		String shadowExpXML = "<shadow type=\"math_number\" id=\"6cwZhokYdk.dI8{3PW-@\">\n"
				+ "<field name=\"NUM\">10</field>\n" + "</shadow>\n";
		Expression shadowExp = Scratch3XMLParser.parseExpressionFromXML(shadowExpXML);

		SetVarBlock setVar3 = SetVarBlock.create("BLOCK_ID","VAR_NAME", "VAR_ID", shadowExp);
		assertEquals("set [VAR_NAME v] to [10]", setVar3.render());
	}

	public void testLookUpBlockID() throws Exception {
		String xml = "<xml>" + 
				"  <variables></variables>\n" + 
				"  <block type=\"motion_movesteps\" id=\"_(!LQ)RNlAl8V2ET.~_f\" x=\"181\" y=\"137\">\n" + 
				"    <value name=\"STEPS\">\n" + 
				"      <shadow type=\"math_number\" id=\"6cwZhokYdk.dI8{3PW-@\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"    <next>\n" + 
				"      <block type=\"motion_turnright\" id=\"`l49h:]ZdA_*3)0k6ZOZ\">\n" + 
				"        <value name=\"DEGREES\">\n" + 
				"          <shadow type=\"math_number\" id=\"_ycP7%_he4WM@EV!sZfM\">\n" + 
				"            <field name=\"NUM\">15</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <next>\n" + 
				"          <block type=\"motion_turnleft\" id=\"l%]K`.h7]:I#}9DR*f_H\">\n" + 
				"            <value name=\"DEGREES\">\n" + 
				"              <shadow type=\"math_number\" id=\"^2u6BH)*5fN:C.ae/q:[\">\n" + 
				"                <field name=\"NUM\">15</field>\n" + 
				"              </shadow>\n" + 
				"            </value>\n" + 
				"          </block>\n" + 
				"        </next>\n" + 
				"      </block>\n" + 
				"    </next>\n" + 
				"  </block>\n" + 
				"  <block type=\"motion_gotoxy\" id=\"=gL-gI[4CMq66G52spsk\" x=\"503\" y=\"148\">\n" + 
				"    <value name=\"X\">\n" + 
				"      <shadow type=\"math_number\" id=\"~1/t?N!7Q@A_up$g!CmI\">\n" + 
				"        <field name=\"NUM\">0</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"    <value name=\"Y\">\n" + 
				"      <shadow type=\"math_number\" id=\"8QNfT@iD3wfb!0rJT!hb\">\n" + 
				"        <field name=\"NUM\">0</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"    <next>\n" + 
				"      <block type=\"motion_glidesecstoxy\" id=\"+^J.IBhjrRRo*a*E[Q]V\">\n" + 
				"        <value name=\"SECS\">\n" + 
				"          <shadow type=\"math_number\" id=\"?O3vy_C%`7jaz7DGo}i6\">\n" + 
				"            <field name=\"NUM\">1</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <value name=\"X\">\n" + 
				"          <shadow type=\"math_number\" id=\"y+W$}dbRb1M!xuxPE+?B\">\n" + 
				"            <field name=\"NUM\">0</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <value name=\"Y\">\n" + 
				"          <shadow type=\"math_number\" id=\"/vG|G$X#WS0g,~J?YA)R\">\n" + 
				"            <field name=\"NUM\">0</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </next>\n" + 
				"  </block>\n" + 
				"</xml>";
		Scriptable sprite = Scratch3XMLParser.parseScriptable(xml);
		String blockID = "+^J.IBhjrRRo*a*E[Q]V";
		ScratchBlock block = sprite.lookupBlockID(blockID);
		assertEquals(blockID, block.getBlockID());
	}
	
	
	public void testLookupNestedBlockID() throws Exception {
		String xml = "<block type=\"motion_movesteps\" id=\"=F@Hl*,a;1rt=RORyi5C\" x=\"171\" y=\"232\">\n" + 
				"    <value name=\"STEPS\">\n" + 
				"      <shadow type=\"math_number\" id=\"U0xx(#3}K*Y0Wr/;XE6L\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"      <block type=\"operator_add\" id=\"ji(UVxkVUh8`u,6Sj=O?\">\n" + 
				"        <value name=\"NUM1\">\n" + 
				"          <shadow type=\"math_number\" id=\"!L0{Rp@D9Q#PsWHZ7h;E\">\n" + 
				"            <field name=\"NUM\">3</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <value name=\"NUM2\">\n" + 
				"          <shadow type=\"math_number\" id=\"mNk)s~MLE%M~tkvui_Kw\">\n" + 
				"            <field name=\"NUM\">4</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </value>\n" + 
				"  </block>";
		CommandBlock commandBlock = (CommandBlock) Scratch3XMLParser.parseBlockFromXML(xml);
		ScratchBlock foundBlock = commandBlock.lookupBlockID("!L0{Rp@D9Q#PsWHZ7h;E");
		assertEquals("(3.0)", foundBlock.render());
	}
	
	/*
	 * TODO: block.lookupBlockID(id)  for control blocks!
	 */

	/*
	 * TODO: Script.insertBlockBefore(Block b, int index);
	 */
	public void testInsertBlockBefore() throws Exception {
		
		Script testScript = null;
		Expression val = new StringLiteral();
		((StringLiteral) val).setValue("A");
		SetVarBlock setVarBlock = SetVarBlock.create("BLOCK_ID", "VAR_NAME", "VAR_ID", val);
		
		testScript = testingBlockSeqInsertionHelper();
		
		CommandBlock targetBlock = (CommandBlock) testScript.lookupBlockID("ID1");
		targetBlock.putBehind(setVarBlock);
		assertEquals(3, testScript.getBody().getNumBlocks());
		assertEquals(0, testScript.getBody().getIndexOfChild(setVarBlock));
		
		testScript = testingBlockSeqInsertionHelper();
		targetBlock = (CommandBlock) testScript.lookupBlockID("ID2");
		targetBlock.putBehind(setVarBlock);
		assertEquals(3, testScript.getBody().getNumBlocks());
		assertEquals(1, testScript.getBody().getIndexOfChild(setVarBlock));		
	}

	private Script testingBlockSeqInsertionHelper() throws Exception {
		Script script = new Script();
		String blockXML = "<block type=\"motion_movesteps\" x=\"181\" y=\"137\">\n" + 
				"    <value name=\"STEPS\">\n" + 
				"      <shadow type=\"math_number\" id=\"6cwZhokYdk.dI8{3PW-@\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"  </block>";
		ScratchBlock b1 = Scratch3XMLParser.parseBlockFromXML(blockXML);
		b1.setBlockID("ID1");
		ScratchBlock b2 = Scratch3XMLParser.parseBlockFromXML(blockXML);
		b2.setBlockID("ID2");
		
		script.addBlock(b1);
		script.addBlock(b2);
		
		return script;
	}
	
	/*
	 * TODO: Expression.replaceWith(Expression e)
	 */
	
	public void testReplaceExpressionWith() throws Exception {
		String commandBlockXML = "<block type=\"motion_movesteps\" x=\"181\" y=\"137\">\n" + 
				"    <value name=\"STEPS\">\n" + 
				"      <shadow type=\"math_number\" id=\"6cwZhokYdk.dI8{3PW-@\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"  </block>";
		
		String expBlockXML = "<block type=\"data_variable\" id=\"`nrmwog$WP|L.azoCCx3\" x=\"101\" y=\"333\">\n" + 
				"    <field name=\"VARIABLE\" id=\"A{]]}?$A.F-}oz=zFGW[\" variabletype=\"\">temp</field>\n" + 
				"  </block>";
		
		CommandBlock commandBlock = (CommandBlock) Scratch3XMLParser.parseBlockFromXML(commandBlockXML);
		assertEquals("move (10.0) steps", commandBlock.render());
		Expression expBlock = (Expression) Scratch3XMLParser.parseExpressionFromXML(expBlockXML);
		ScratchBlock numShadowBlock = commandBlock.lookupBlockID("6cwZhokYdk.dI8{3PW-@");
		numShadowBlock.replaceWith(expBlock);
		assertEquals("move (temp::variables) steps", commandBlock.render());
	}
	
}
