package analysis;

import java.io.StringReader;

import junit.framework.TestCase;
import analysis.ast.CommandBlock;
import analysis.ast.Program;
import analysis.ast.Script;
import analysis.ast.SetVarBlock;
import analysis.ast.VariableExpBlock;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;

public class VariableParsingTest extends TestCase {
	private final static String FILENAME = "/parsing/test-var.json";
	private final static int projectID = 181232855;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		ScratchParser parser = new ScratchParser();
		try {
			srd = Util.retrieveProjectOnline(projectID);
//			srd = Util.getStringReaderFromFile(FILENAME);
			program = parser.parse(srd);
		} catch (Throwable e) {
			e.printStackTrace();
		}
		
	}

	public void testVarAccessInSetAndChangeVarBlock() {
		assertEquals(1, program.getStage().getNumVarDecl());
		assertEquals(1, program.getStage().getNumSprite());
		assertEquals(1, program.getStage().getSprite(0).getNumVarDecl());
		
		//rendering
		Script script = program.getStage().getSprite(0).getScript(0);
		SetVarBlock blk = (SetVarBlock) script.getBody().getBlocks(0);
		assertEquals("set [privateB v] to (0)",blk.render());
	}
	
	public void testVarAccessInVarExpBlock() {
		Script script = program.getStage().getSprite(0).getScript(1);
		CommandBlock move = (CommandBlock) script.getBody().getBlocks(0);
		VariableExpBlock blk = (VariableExpBlock) move.getArgument(0);
		System.out.println(blk.render());
	}
	
	public void testVarExpBlockStandAlone() {
		Script script = program.getStage().getSprite(0).getScript(2);
		VariableExpBlock blk = (VariableExpBlock) script.getBody().getBlocks(0);
		System.out.println(blk.render());
		
	}
}