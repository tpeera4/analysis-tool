package analysis.pattern;

import java.io.StringReader;

import junit.framework.TestCase;
import analysis.ast.Program;
import analysis.parsing.ScratchParser;
import analysis.parsing.Util;
import analysis.practices.ExplainingVarAnalysis;

public class ExplainingVarTest extends TestCase {
	private final static String FILENAME = "/pattern/test-ExplainingVar.json";
	private final static int projectID = 174582969;
	Program program = null;
	
	@Override
	protected void setUp() {
		StringReader srd = null;
		  ScratchParser parser = new ScratchParser();
	      program = null;
	      
	      try {
//	    	  srd = Util.retrieveProjectOnline(projectID);
	          srd = Util.getStringReaderFromFile(FILENAME);
	          program = parser.parse(srd);
	      } catch (Throwable e) {
	          e.printStackTrace();
	      }
	}
	
	public void testExplainingVarAnalysisIsWorking() {
		ExplainingVarAnalysis analysis = new ExplainingVarAnalysis();
		analysis.setInputProgram(program);
		analysis.performAnalysis();
		assertNotNull(analysis.getShortReportAsJson());
		assertEquals(1, analysis.getShortReportAsJsonObject().get("count").getAsInt());
		System.out.println(analysis.getShortReportAsJson());
	}
	public void testComplexExpAssignment() {
		
	}
}