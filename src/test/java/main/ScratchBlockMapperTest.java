package main;

import analysis.parsing.ScratchBlocksMapper;
import junit.framework.TestCase;

public class ScratchBlockMapperTest extends TestCase {

	public void testSb2Spec() {
		assertTrue(ScratchBlocksMapper.name2id.size() > 0);
	}

	public void test2WayMappingBetweenSb2ToSb3() {
		assertTrue(ScratchBlocksMapper.sb2tosb3.size() > 0);
		assertTrue(ScratchBlocksMapper.sb3tosb2.size() > 0);
	}

}
