package main;


import java.io.IOException;
import java.util.List;


import analysis.ast.Script;
import analysis.ast.VarDecl;
import analysis.parsing.scratch3.Scratch3XMLParser;
import junit.framework.TestCase;
import nu.xom.Builder;
import nu.xom.Document;
import nu.xom.Element;
import nu.xom.ParsingException;
import nu.xom.ValidityException;

public class Scratch3XMLParserTest extends TestCase {
	public void testParseSimpleBlock() throws Exception {
		String xml = "<xml >\n" + 
				"  <variables>\n" + 
				"    <variable type=\"broadcast_msg\" id=\"|laxgT16QNB?OOG+{|kw\">message1</variable>\n" + 
				"  </variables>\n" + 
				"  <block type=\"motion_movesteps\" id=\"`N4eT;?4(DA#g0OLd,A9\" x=\"5\" y=\"11\">\n" + 
				"    <value name=\"STEPS\">\n" + 
				"      <shadow type=\"math_number\" id=\"9J^)0x1nd)!G5+`AH^4a\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"  </block>\n" + 
				"</xml>";
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		assertEquals("move (10.0) steps",scripts.get(0).getBody().getBlocks(0).render());
		
	}
	
	public void testParseScript() throws Exception {
		String xml = "<xml>\n" + 
				"  <variables></variables>\n" + 
				"  <block type=\"event_whenflagclicked\" id=\"[4KqGUlosAf1$*^u/yIJ\" x=\"40\" y=\"17\">\n" + 
				"    <next>\n" + 
				"      <block type=\"motion_movesteps\" id=\"BbRs1XQgO?w@~{Ri$q;s\">\n" + 
				"        <value name=\"STEPS\">\n" + 
				"          <shadow type=\"math_number\" id=\"@7KEUkx9+R/jchp=D,R4\">\n" + 
				"            <field name=\"NUM\">10</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <next>\n" + 
				"          <block type=\"motion_glidesecstoxy\" id=\":=$HTTU=?4$v~F8dm;K(\">\n" + 
				"            <value name=\"SECS\">\n" + 
				"              <shadow type=\"math_number\" id=\"0_s0d{h8T[Nf[.@@jn5K\">\n" + 
				"                <field name=\"NUM\">1</field>\n" + 
				"              </shadow>\n" + 
				"            </value>\n" + 
				"            <value name=\"X\">\n" + 
				"              <shadow type=\"math_number\" id=\"?E]-fc76/w?kmCK`N%A[\">\n" + 
				"                <field name=\"NUM\">0</field>\n" + 
				"              </shadow>\n" + 
				"            </value>\n" + 
				"            <value name=\"Y\">\n" + 
				"              <shadow type=\"math_number\" id=\"hiyhw~:k~cWnSq7rHP}i\">\n" + 
				"                <field name=\"NUM\">0</field>\n" + 
				"              </shadow>\n" + 
				"            </value>\n" + 
				"          </block>\n" + 
				"        </next>\n" + 
				"      </block>\n" + 
				"    </next>\n" + 
				"  </block>\n" + 
				"</xml>";
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
	}
	
	public void testParseExpressionBlockWithMultipleInputs() throws Exception {
		String xml = "<xml>\n" + 
				"  <variables></variables>\n" + 
				"  <block type=\"operator_add\" id=\"iW_o14|dbAIPRSh9c^-H\" x=\"03\" y=\"1\">\n" + 
				"    <value name=\"NUM1\">\n" + 
				"      <shadow type=\"math_number\" id=\"dmy+:+%+!6abPs6g/b7@\">\n" + 
				"        <field name=\"NUM\"></field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"    <value name=\"NUM\">\n" + 
				"      <shadow type=\"math_number\" id=\"AuTip1`5UFuhfVkCz[eE\">\n" + 
				"        <field name=\"NUM\"></field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"  </block>\n" + 
				"</xml>";
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
	}
	
	public void testParseBlockWithBlockAsInput() throws  Exception {
		String xml = "<xml>\n" + 
				"  <variables></variables>\n" + 
				"  <block type=\"motion_movesteps\" id=\"?d$=yXn4E5GNE=l55m;p\" x=\"01\" y=\"-68\">\n" + 
				"    <value name=\"STEPS\">\n" + 
				"      <shadow type=\"math_number\" id=\"^~jbRnz_vb}G]`SXdo!\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"      <block type=\"operator_round\" id=\".u]#?/W!KZ3t^SBR9;.Y\">\n" + 
				"        <value name=\"NUM\">\n" + 
				"          <shadow type=\"math_number\" id=\":n:.U./YAL!o(1PBe@{w\">\n" + 
				"            <field name=\"NUM\">5</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </value>\n" + 
				"  </block>\n" + 
				"</xml>";
		
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
	}
	
	
	public void testParseDoRepeat() throws  Exception {
		String xml = "<xml>\n" + 
				"  <variables></variables>\n" + 
				"  <block type=\"control_repeat\" id=\"L0;G_yB|=m4fvQ-Cs#%\" x=\"139\" y=\"155\">\n" + 
				"    <value name=\"TIMES\">\n" + 
				"      <shadow type=\"math_whole_number\" id=\"N`i``^Y-_]rPu^u1,ap[\">\n" + 
				"        <field name=\"NUM\">10</field>\n" + 
				"      </shadow>\n" + 
				"    </value>\n" + 
				"    <statement name=\"SUBSTACK\">\n" + 
				"      <block type=\"motion_movesteps\" id=\"Wv]@ECax:L)Uyn#[R=`\">\n" + 
				"        <value name=\"STEPS\">\n" + 
				"          <shadow type=\"math_number\" id=\"mR(XDC!I}p8BD/C_K1`P\">\n" + 
				"            <field name=\"NUM\">10</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </statement>\n" + 
				"  </block>\n" + 
				"</xml>";
		
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
	}
	
	public void testParseDoIf() throws  Exception {
		String xml = "<xml>\n" + 
				"  <variables></variables>\n" + 
				"  <block type=\"control_if\" id=\"Ddb`tgd+9Q#[BE$B%CBK\" x=\"181\" y=\"15\">\n" + 
				"    <value name=\"CONDITION\">\n" + 
				"      <block type=\"operator_lt\" id=\"ox_ESM+v(u,[$j3ir$W;\">\n" + 
				"        <value name=\"OPERAND1\">\n" + 
				"          <shadow type=\"text\" id=\"NNB|],fhDR6@Jmrp[!d\">\n" + 
				"            <field name=\"TEXT\">1</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <value name=\"OPERAND\">\n" + 
				"          <shadow type=\"text\" id=\"AErf^6%07dOr,C|*t4v\">\n" + 
				"            <field name=\"TEXT\"></field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </value>\n" + 
				"    <statement name=\"SUBSTACK\">\n" + 
				"      <block type=\"motion_movesteps\" id=\"3;A~nhToIq(dcKkN*CMs\">\n" + 
				"        <value name=\"STEPS\">\n" + 
				"          <shadow type=\"math_number\" id=\"h!RFkonVCV+Bw7w.^^oq\">\n" + 
				"            <field name=\"NUM\">10</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </statement>\n" + 
				"  </block>\n" + 
				"</xml>";
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
	}
	
	public void testParseDoIfThenElse() throws  Exception {
		String xml = "<xml>\n" + 
				"  <variables></variables>\n" + 
				"  <block type=\"control_if_else\" id=\"p#/4qK;qht*anNk~JZ96\" x=\"193\" y=\"135\">\n" + 
				"    <value name=\"CONDITION\">\n" + 
				"      <block type=\"operator_lt\" id=\"ox_ESM+v(u,[$j3ir$W;\">\n" + 
				"        <value name=\"OPERAND1\">\n" + 
				"          <shadow type=\"text\" id=\"NNB|],fhDR6@Jmrp[!d\">\n" + 
				"            <field name=\"TEXT\">1</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"        <value name=\"OPERAND\">\n" + 
				"          <shadow type=\"text\" id=\"AErf^6%07dOr,C|*t4v\">\n" + 
				"            <field name=\"TEXT\"></field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </value>\n" + 
				"    <statement name=\"SUBSTACK\">\n" + 
				"      <block type=\"motion_movesteps\" id=\"3;A~nhToIq(dcKkN*CMs\">\n" + 
				"        <value name=\"STEPS\">\n" + 
				"          <shadow type=\"math_number\" id=\"h!RFkonVCV+Bw7w.^^oq\">\n" + 
				"            <field name=\"NUM\">10</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </statement>\n" + 
				"    <statement name=\"SUBSTACK\">\n" + 
				"      <block type=\"motion_turnright\" id=\"+X{z$CUql@*eGv(LtS8\">\n" + 
				"        <value name=\"DEGREES\">\n" + 
				"          <shadow type=\"math_number\" id=\"|S/pj0s~e85CPLU)IK#\">\n" + 
				"            <field name=\"NUM\">15</field>\n" + 
				"          </shadow>\n" + 
				"        </value>\n" + 
				"      </block>\n" + 
				"    </statement>\n" + 
				"  </block>\n" + 
				"</xml>";
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
		
	}
	
	public void testIgnoreShadowTopElement() throws  Exception {
		String xml = "<xml>\n" + 
				"	<variables>\n" + 
				"		<variable type=\"\" id=\"Lj9,v0lkTjv(wvrA0KR6-my variable\">my variable</variable>\n" + 
				"	</variables>\n" + 
				"	<block\n" + 
				"                id=\"Ny)A4K7:C.wSjl3R/z;(\"\n" + 
				"                type=\"motion_movesteps\"\n" + 
				"                x=\"84.5959595955\" y=\"130.51851851851853\"\n" + 
				"            >\n" + 
				"		<value name=\"STEPS\">\n" + 
				"			<block\n" + 
				"                id=\"a/3/}Ac8j~9sj5-{u_qN\"\n" + 
				"                type=\"operator_round\"\n" + 
				"                \n" + 
				"            >\n" + 
				"				<value name=\"NUM\">\n" + 
				"					<shadow\n" + 
				"                id=\"}THO#0(dQ;p8t1h)]$C\"\n" + 
				"                type=\"math_number\"\n" + 
				"                \n" + 
				"            >\n" + 
				"						<field name=\"NUM\"></field>\n" + 
				"					</shadow>\n" + 
				"				</value>\n" + 
				"			</block>\n" + 
				"			<shadow\n" + 
				"                id=\"dn9V!5H5)4B@=HoKjjd#\"\n" + 
				"                type=\"math_number\"\n" + 
				"                x=\"139.706934599177\" y=\"138.51851851851853\"\n" + 
				"            >\n" + 
				"				<field name=\"NUM\">10</field>\n" + 
				"			</shadow>\n" + 
				"		</value>\n" + 
				"	</block>,\n" + 
				"	<shadow\n" + 
				"                id=\"dn9V!5H5)4B@=HoKjjd#\"\n" + 
				"                type=\"math_number\"\n" + 
				"                x=\"139.706934599177\" y=\"138.51851851851853\"\n" + 
				"            >\n" + 
				"		<field name=\"NUM\">10</field>\n" + 
				"	</shadow>\n" + 
				"</xml>";
		List<Script> scripts = Scratch3XMLParser.parse(xml);
		for(Script s : scripts) {
			System.out.println(s.render());
		}
	}
	
	public void testParseVariableList() throws ValidityException, ParsingException, IOException {
		String xml = "  <variables>\n" + 
				"    <variable type=\"\" id=\"]uuT|^v^s}6)q~p;^ph,\">a</variable>\n" + 
				"    <variable type=\"\" id=\"L=G(66zM1ILC4hn}GSc\">b</variable>\n" + 
				"  </variables>\n" ;
		Builder parser = new Builder();
		Document doc = parser.build(xml, null);
	
		Element root = doc.getRootElement();
		List<VarDecl> vars = Scratch3XMLParser.parseVariables(root);
		assertEquals(2 , vars.size());
		
	}
}
