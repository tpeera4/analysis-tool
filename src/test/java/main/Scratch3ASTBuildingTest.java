package main;

import java.io.FileNotFoundException;
import java.io.StringReader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import analysis.ast.Program;
import analysis.ast.ScratchBlock;
import analysis.ast.Script;
import analysis.ast.Scriptable;
import analysis.parsing.Util;
import analysis.parsing.scratch3.Scratch3ASTBuilder;
import junit.framework.TestCase;

public class Scratch3ASTBuildingTest extends TestCase {
	JsonParser jsonParser = new JsonParser();
	Scratch3ASTBuilder builder = new Scratch3ASTBuilder();

	public void testParseProgram() throws FileNotFoundException {
		StringReader srd = Util.getStringReaderFromFile("/scratch3/empty-project.json");
		JsonObject jsonProgramObj = jsonParser.parse(srd).getAsJsonObject();
		Program program = builder.parseProgram(jsonProgramObj);
		assertEquals(0, program.getTotalNumberOfScript());
	}

	public void testParseSimpleProgram() throws FileNotFoundException {
		StringReader srd = Util.getStringReaderFromFile("/scratch3/simple-project.json");
		JsonObject jsonProgramObj = jsonParser.parse(srd).getAsJsonObject();
		Program program = builder.parseProgram(jsonProgramObj);
		assertEquals(2, program.getStage().getSprite(0).getNumScript());
		assertEquals(2, program.getStage().getSprite(0).getScript(0).getBody().getNumBlocks());
		assertEquals(1, program.getStage().getSprite(0).getScript(1).getBody().getNumBlocks());
	}

	public void testParseABlockWithExpressionBlockInput() throws FileNotFoundException {
		StringReader srd = Util.getStringReaderFromFile("/scratch3/expression-block.json");
		JsonObject jsonProgramObj = jsonParser.parse(srd).getAsJsonObject();
		Program program = builder.parseProgram(jsonProgramObj);
		ScratchBlock blockWithExpBlockAsInput = program.getStage().getSprite(0).getScript(0).getBody().getBlocks(1);
		assertEquals("move ((3.0) + (4.0)) steps", blockWithExpBlockAsInput.render());
	}

	public void testParseBlockWithSubStack() throws FileNotFoundException {
		StringReader srd = Util.getStringReaderFromFile("/scratch3/block-with-substack.json");
		JsonObject jsonProgramObj = jsonParser.parse(srd).getAsJsonObject();
		Program program = builder.parseProgram(jsonProgramObj);
		Script script = program.getStage().getSprite(0).getScript(0);
		assertTrue(script.render().contains("repeat"));
		assertTrue(script.render().contains("turn"));
	}

}
